#include "AIStates.h"
#include <stdio.h>
#include "Globals.h"
//creates one global state to use
searchingForGoal * searchingForGoal::Instance()
{
	static searchingForGoal instance;
	return &instance;
}
//checks if state should change based on factors, if not paused, move towards the goal.
void searchingForGoal::Execute(AIcontroller* bot)
{
	if (bot->foundGoal())
	{
		bot->ChangeState(foundGoal::Instance());
	}
	else if (bot->dangerNear())
	{
		bot->ChangeState(fleeingFromDanger::Instance());
	}
	else if(!bot->worldPause)
	{
		bot->moveTowardGoal();
	}
}
//telling us what it's doing in this state
void searchingForGoal::Enter(AIcontroller* bot)
{
	printf("It's time to search for the goal!\n");
}
//telling us it's leaving the state
void searchingForGoal::Exit(AIcontroller* bot)
{
	printf("Something has gotten in the way of my search!\n");
}
//creates one global state to use
fleeingFromDanger * fleeingFromDanger::Instance()
{
	static fleeingFromDanger instance;
	return &instance;
}
//checks if we're out of danger, or if we've stumbled onto the goal while fleeing, otherwise will continue to flee
void fleeingFromDanger::Execute(AIcontroller* bot)
{
	if (bot->escapedDanger())
	{
		bot->ChangeState(searchingForGoal::Instance());
	}
	else if (bot->foundGoal())
	{
		bot->ChangeState(foundGoal::Instance());
	}
	else
	{
		bot->flee();
	}
}
//tells us it's being a coward and running away
void fleeingFromDanger::Enter(AIcontroller* bot)
{
	printf("Discovered enemy nearby, relocating...\n");
}
//when it's safe, lets us know
void fleeingFromDanger::Exit(AIcontroller* bot)
{
	printf("Phew, that was close...\n");
}
//global instance
foundGoal * foundGoal::Instance()
{
	static foundGoal instance;
	return &instance;
}
//pauses itself when it finds the goal
void foundGoal::Execute(AIcontroller * bot)
{
	if (!bot->worldPause)
	{
		bot->worldPause = true;
	}
}
//tells us the goal is found and where it is
void foundGoal::Enter(AIcontroller * bot)
{
	printf("Found the goal! It's at the following co-ords: (%d, %d)\n", bot->Pos.x, bot->Pos.y);
}
//for this project, goal state is program end, so we never need to exit
void foundGoal::Exit(AIcontroller * bot)
{
}
//global state
enemyState * enemyState::Instance()
{
	static enemyState instance;
	return &instance;
}
//enemy state for enemy AIs only. Ais move randomly when unpaused
void enemyState::Execute(AIcontroller * bot)
{
	if (!bot->enemyPause)
	{
		bot->moveRandomly();
	}
}
//enemy AIs never enter or exit this state, no code required.
void enemyState::Enter(AIcontroller * bot)
{
}

void enemyState::Exit(AIcontroller * bot)
{
}
