#pragma once
#include "State.h"
//holds state information
class AIcontroller;

//each class is described by its name
//contains constructor, execute, enter and exit functions
//execute runs logic required while in that state, enter runs any code required for that state when entering, exit runs code that may need to run when done with that state
class searchingForGoal : public State
{
private:

public:
	searchingForGoal() {};
	//singleton

	static searchingForGoal* Instance();

	virtual void Execute(AIcontroller*);
	virtual void Enter(AIcontroller*);
	virtual void Exit(AIcontroller*);
};


class fleeingFromDanger : public State
{
private:

public:
	fleeingFromDanger() {};

	static fleeingFromDanger* Instance();

	virtual void Execute(AIcontroller*);
	virtual void Enter(AIcontroller*);
	virtual void Exit(AIcontroller*);
};

class foundGoal : public State
{
private:
public:
	foundGoal() {};

	static foundGoal* Instance();

	virtual void Execute(AIcontroller*);
	virtual void Enter(AIcontroller*);
	virtual void Exit(AIcontroller*);
};

class enemyState : public State
{
private:

public:
	enemyState() {};
	//singleton

	static enemyState* Instance();

	virtual void Execute(AIcontroller*);
	virtual void Enter(AIcontroller*);
	virtual void Exit(AIcontroller*);
};