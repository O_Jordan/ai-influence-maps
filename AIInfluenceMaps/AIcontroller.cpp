#include "AIcontroller.h"
#include "AIStates.h"
#include <stdlib.h>
#include <time.h>
//sets appropriate attributes for Player AI
AIcontroller::AIcontroller(Map* inMap, bool isEnemy)
{
	map = inMap;
	pCurrentState = searchingForGoal::Instance();
	worldPause = true;
	enemy = isEnemy;
	//set seed for random functions
	srand(time(NULL));
}
//default constructor sets up Enemy AI
AIcontroller::AIcontroller()
{
	pCurrentState = enemyState::Instance();
	enemyPause = true;
	//set seed for random functions
	srand(time(NULL));
}
//update executes current state's execute function
void AIcontroller::Update()
{
	pCurrentState->Execute(this);
}
//checks if player is on same tile as goal
bool AIcontroller::foundGoal()
{
	if (Pos.x == Goal.x && Pos.y == Goal.y)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//checks every surrounding tile to see if 1) enemy influence is high enough to be a danger and 2) it's not a wall. returns true if in danger
bool AIcontroller::dangerNear()
{
	if (map->tileList[(Pos.y - 1)*map->width + Pos.x].infIndex[1] > 90 && map->tileList[(Pos.y - 1)*map->width + Pos.x].infIndex[0] != 1000)  //up
	{
		return true;
	}
	else if (map->tileList[(Pos.y - 1)*map->width + Pos.x - 1].infIndex[1] > 90 && map->tileList[(Pos.y - 1)*map->width + Pos.x - 1].infIndex[0] != 1000) //up left
	{
		return true;
	}
	else if (map->tileList[(Pos.y - 1)*map->width + Pos.x + 1].infIndex[1] > 90 && map->tileList[(Pos.y - 1)*map->width + Pos.x + 1].infIndex[0] != 1000) //up right
	{
		return true;
	}
	else if (map->tileList[Pos.y*map->width + Pos.x - 1].infIndex[1] > 90 && map->tileList[Pos.y*map->width + Pos.x - 1].infIndex[0] != 1000) //left
	{
		return true;
	}
	else if (map->tileList[Pos.y*map->width + Pos.x + 1].infIndex[1] > 90 && map->tileList[Pos.y*map->width + Pos.x + 1].infIndex[0] != 1000) //right
	{
		return true;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x].infIndex[1] > 90 && map->tileList[(Pos.y + 1)*map->width + Pos.x].infIndex[0] != 1000) //down
	{
		return true;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x + 1].infIndex[1] > 90 && map->tileList[(Pos.y + 1)*map->width + Pos.x + 1].infIndex[0] != 1000) //up right
	{
		return true;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x - 1].infIndex[1] > 90 && map->tileList[(Pos.y + 1)*map->width + Pos.x - 1].infIndex[0] != 1000) //up left
	{
		return true;
	}
	else
	{
		return false;
	}
}

//checks every surrounding tile to see if danger is not near anymore, or if there is a wall in the way. Returns true if there's no longer danger
bool AIcontroller::escapedDanger()
{
	if (map->tileList[(Pos.y - 1)*map->width + Pos.x].infIndex[1] > 90 && map->tileList[(Pos.y - 1)*map->width + Pos.x].infIndex[0] != 1000)  //up
	{
		return false;
	}
	else if (map->tileList[(Pos.y - 1)*map->width + Pos.x - 1].infIndex[1] > 90 && map->tileList[(Pos.y - 1)*map->width + Pos.x - 1].infIndex[0] != 1000) //up left
	{
		return false;
	}
	else if (map->tileList[(Pos.y - 1)*map->width + Pos.x + 1].infIndex[1] > 90 && map->tileList[(Pos.y - 1)*map->width + Pos.x + 1].infIndex[0] != 1000) //up right
	{
		return false;
	}
	else if (map->tileList[Pos.y*map->width + Pos.x - 1].infIndex[1] > 90 && map->tileList[Pos.y*map->width + Pos.x - 1].infIndex[0] != 1000) //left
	{
		return false;
	}
	else if (map->tileList[Pos.y*map->width + Pos.x + 1].infIndex[1] > 90 && map->tileList[Pos.y*map->width + Pos.x + 1].infIndex[0] != 1000) //right
	{
		return false;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x].infIndex[1] > 90 && map->tileList[(Pos.y + 1)*map->width + Pos.x].infIndex[0] != 1000) //down
	{
		return false;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x + 1].infIndex[1] > 90 && map->tileList[(Pos.y + 1)*map->width + Pos.x + 1].infIndex[0] != 1000) //up right
	{
		return false;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x - 1].infIndex[1] > 90 && map->tileList[(Pos.y + 1)*map->width + Pos.x - 1].infIndex[0] != 1000) //up left
	{
		return false;
	}
	else
	{
		return true;
	}
}
//changes to specified state, runs exit of current state and enter of new state
void AIcontroller::ChangeState(State* nextState)
{
	pCurrentState->Exit(this);
	pCurrentState = nextState;
	pCurrentState->Enter(this);
}

//will move toward goal based on goal's influence.
void AIcontroller::moveTowardGoal()
{
	//storing each tile data to be able to sort
	Map::tile * tiles[8];
	tiles[0] = &map->tileList[(Pos.y - 1)*map->width + Pos.x];
	tiles[1] = &map->tileList[(Pos.y - 1)*map->width + Pos.x - 1];
	tiles[2] = &map->tileList[(Pos.y - 1)*map->width + Pos.x + 1];
	tiles[3] = &map->tileList[Pos.y*map->width + Pos.x - 1];
	tiles[4] = &map->tileList[Pos.y*map->width + Pos.x + 1];
	tiles[5] = &map->tileList[(Pos.y + 1)*map->width + Pos.x];
	tiles[6] = &map->tileList[(Pos.y + 1)*map->width + Pos.x + 1];
	tiles[7] = &map->tileList[(Pos.y + 1)*map->width + Pos.x - 1];
	//bubblesort least to most visited
	while (1)
	{
		bool swapped = false;
		for (int i = 0; i < 7; i++)
		{
			Map::tile* temp;
			if (tiles[i + 1]->visited < tiles[i]->visited)
			{
				temp = tiles[i + 1];
				tiles[i + 1] = tiles[i];
				tiles[i] = temp;
				swapped = true;
			}
		}
		if (!swapped)
		{
			break;
		}
	}
	//End bubble sort
	//bool maybestuck flips true if there isn't a tile the AI can reach by following the goal's influence
	bool maybeStuck = false;
	//index pointing to which tile in our lil array is the least visited and not a wall
	int leastNotWall;
	for (int i = 0; i < 7; i++)
	{
		if (tiles[i]->infIndex[0] != 1000)
		{
			//sets least visited and not wall index
			leastNotWall = i;
			break;
		}
	}
	//flips to true provided the AI has found a tile to move to
	bool moved = false;
	//for each surrounding tile...
	for (int i = 0; i < 8; i++)
	{
		//if the tile's goal influence value is higher than the tile we're on...
		if ((tiles[i]->infIndex[2] - tiles[i]->infIndex[0]) > map->tileList[(Pos.y)*map->width + Pos.x].infIndex[2])
		{
			//and that tile has not yet been visited more than 50 times...
			if (tiles[i]->visited < 50)
			{
				//step off current tile and increment visited count
				map->tileList[(Pos.y)*map->width + Pos.x].visited++;
				Pos.x = tiles[i]->position.x;
				Pos.y = tiles[i]->position.y;
				//move has been made, flip to true
				moved = true;
				//we can break early out of the loop if moving based on influence. We always visit the least visited tile with higher influence by sorting tiles before moving based on influence.
				break;
			}
		}
	}
	//if none of the tiles were eligible...
	if (!moved)
	{
		//check each tile...
		for (int i = 0; i < 8; i++)
		{
			//if it's not a wall...
			if (tiles[i]->infIndex[0] != 1000)
			{
				//move to this tile and increment visited counts
				map->tileList[(Pos.y)*map->width + Pos.x].visited++;
				Pos.x = tiles[i]->position.x;
				Pos.y = tiles[i]->position.y;
				//break out here... since tiles are sorted, when no influence based move is made, we step onto the least visited tile regardless of it's influence value
				break;
			}
		}
	}
}

//function picks a random direction, checks it's not a wall, and moves an AI to it.
void AIcontroller::moveRandomly()
{
	bool success = false;
	while (!success)
	{
		int direction = rand() % 8 + 1;
		switch (direction)
		{
		case 1: // up
			if (map->tileList[((Pos.y - 1) * map->width) + Pos.x].infIndex[0] != 1000)
			{
				Pos.y = Pos.y - 1;
				success = true;
			}
			break;
		case 2: //up right
			if (map->tileList[((Pos.y - 1) * map->width) + Pos.x + 1].infIndex[0] != 1000)
			{
				Pos.y = Pos.y - 1;
				Pos.x = Pos.x + 1;
				success = true;
			}
			break;
		case 3: // right
			if (map->tileList[(Pos.y * map->width) + Pos.x + 1].infIndex[0] != 1000)
			{
				Pos.x = Pos.x + 1;
				success = true;
			}
			break;
		case 4: //right down
			if (map->tileList[((Pos.y + 1) * map->width) + Pos.x + 1].infIndex[0] != 1000)
			{
				Pos.x = Pos.x + 1;
				Pos.y = Pos.y + 1;
				success = true;
			}
			break;
		case 5: //down
			if (map->tileList[((Pos.y + 1) * map->width) + Pos.x].infIndex[0] != 1000)
			{
				Pos.y = Pos.y + 1;
				success = true;
			}
			break;
		case 6: //down left
			if (map->tileList[((Pos.y + 1) * map->width) + Pos.x - 1].infIndex[0] != 1000)
			{
				Pos.y = Pos.y + 1;
				Pos.x = Pos.x - 1;
				success = true;
			}
			break;
		case 7: // left
			if (map->tileList[(Pos.y * map->width) + Pos.x - 1].infIndex[0] != 1000)
			{
				Pos.x = Pos.x - 1;
				success = true;
			}
			break;
		case 8: //up left
			if (map->tileList[((Pos.y - 1) * map->width) + Pos.x - 1].infIndex[0] != 1000)
			{
				Pos.x = Pos.x - 1;
				Pos.y = Pos.y - 1;
				success = true;
			}
			break;
		}
	}
}

//checks surrounding tiles for an enemy influence lower than the one it's on, check it's not a wall, move to that tile.
void AIcontroller::flee()
{
	if (map->tileList[(Pos.y - 1)*map->width + Pos.x].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[(Pos.y - 1)*map->width + Pos.x].infIndex[0] != 1000)  //up
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.y = Pos.y - 1;
	}
	else if (map->tileList[(Pos.y - 1)*map->width + Pos.x - 1].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[(Pos.y - 1)*map->width + Pos.x - 1].infIndex[0] != 1000) //up left
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.y = Pos.y - 1;
		Pos.x = Pos.x - 1;
	}
	else if (map->tileList[(Pos.y - 1)*map->width + Pos.x + 1].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[(Pos.y - 1)*map->width + Pos.x + 1].infIndex[0] != 1000) //up right
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.y = Pos.y - 1;
		Pos.x = Pos.x + 1;
	}
	else if (map->tileList[Pos.y*map->width + Pos.x - 1].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[Pos.y*map->width + Pos.x - 1].infIndex[0] != 1000) //left
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.x = Pos.x - 1;
	}
	else if (map->tileList[Pos.y*map->width + Pos.x + 1].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[Pos.y*map->width + Pos.x + 1].infIndex[0] != 1000) //right
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.x = Pos.x + 1;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[(Pos.y + 1)*map->width + Pos.x].infIndex[0] != 1000) //down
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.y = Pos.y + 1;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x + 1].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[(Pos.y + 1)*map->width + Pos.x + 1].infIndex[1] != 1000) //up right
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.y = Pos.y + 1;
		Pos.x = Pos.x + 1;
	}
	else if (map->tileList[(Pos.y + 1)*map->width + Pos.x - 1].infIndex[1] < map->tileList[(Pos.y)*map->width + Pos.x].infIndex[1] && map->tileList[(Pos.y + 1)*map->width + Pos.x - 1].infIndex[0] != 1000) //up left
	{
		map->tileList[(Pos.y)*map->width + Pos.x].visited++;
		Pos.y = Pos.y + 1;
		Pos.x = Pos.x - 1;
	}
}
