#include "Map.h"
#include "AIStates.h"
class State;
//STATE MACHINE AI CONTROLLER
class AIcontroller
{
private:
	//stores current state
	State* pCurrentState;

public:
	//constructor using map to initialise
	AIcontroller(Map* inMap, bool isEnemy);
	//default constructor sets up as if it's an enemy AI
	AIcontroller();
	//stores loaded map
	Map* map;

	//world pause will keep a Player from executing in searching state
	bool worldPause;
	//enemy pause will keep an Enemy from executing in enemy state
	bool enemyPause;
	//variable describing if AI is player or enemy
	bool enemy;

	//position structure
	struct position
	{
		int x = 0;
		int y = 0;
	};

	//stores AI position
	position Pos;
	//stores goal position, used for win condition checking
	position Goal;
	void Update(); //call this each time step

	//state functions
	//checks if goal is found
	bool foundGoal();
	//checks for danger
	bool dangerNear();
	//checks if out of danger
	bool escapedDanger();

	//function for changing state
	void ChangeState(State* nextState);

	//additional logic functions
	//moves AI toward goal
	void moveTowardGoal();
	//moves AI randomly (used only by enemy AI)
	void moveRandomly();
	//runs from danger
	void flee();
};
