#include "Globals.h"


//set const math number
const int SCREEN_HEIGHT = 1080;
const int SCREEN_WIDTH = 1920;
int TIMER = 10;
const float e = 2.178;
//infViewMode defines which influences are being shown
int infViewMode = 0;
//setting up blank objects ready to be loaded into
obj Ground(1, 1, 1, 1);
obj Wall(1, 1, 1, 1);
obj Entity(1, 1, 1, 1);
obj Goal(1, 1, 1, 1);
//setting up blank map ready to be loaded into
Map map(1, 1, 1, 1);
//AIcontroller for player initialised with pointer to the map and a flag detailing if it's an enemy set to false (not truly used for anything, but available for possible expansion)
AIcontroller AIPlayer(&map, false);
//set an array of AIs to be used for enemy entities
AIcontroller * enemies;

//camera control variables...
float strafe = 0.0f;
float fly = 0.0f;
float lookAtX = 1.0f;
float lookAtY = 1.0f;
float lookAtZ = 1.0f;
float posX = 20.0f;
float posY = 10.0f;
float posZ = 20.0f;

//matricies for rotating forward facing vector to find left and right vector for strafing
Matrix rot90;
Matrix rot270;
//math lib (used from previous work) and vectors to hold relevant directions
MathLib maths;
//setting vectors to store left right and forward vectors
Vector right(1,1,1);
Vector left(1,1,1);
Vector fwd(1, 1, 1);
//setting vectors to store rotation values
int yAxisRotTot = 0;
int xAxisRotTot = 0;
//general rotation matrix to be used to create infrequently used rotation matrices
Matrix rotation;
Vector newLookAt(1, 1, 1);
//float to hold length for normalisation
float length;
//function prototypes for influence calculations
void calcInf(Map* inMap);
void cleanEnemyInf(Map* inMap);
//prototype function for game loop, called repeatedly using glutTimerFunc
void round(int n);
//function called when mouse moves, rotates camera
void rotCam(int x, int y);
//bool used to lock and unlock the mouse from camera controls
bool lockMouse = true;

//round is the game loop, called repeatedly from the moment the program starts. AI updates and influence calculation happen in here.
void round(int n)
{
	//clean enemy influence from map
	cleanEnemyInf(&map);
	//calculate influences again
	calcInf(&map);
	//update player
	AIPlayer.Update();
	//update every enemy
	for (int i = 0; i < map.enemies; i++)
	{
		enemies[i].Update();
	}
	if (lockMouse)
	{
		
		glutWarpPointer(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	}
	//redisplay
	glutPostRedisplay();
	//call again after 10ms
	glutTimerFunc(TIMER, round, 0);
}

void rotCam(int x, int y)
{
	if (lockMouse)
	{
		int distMidX = (SCREEN_WIDTH / 2) - x;
		if (yAxisRotTot != 360 || yAxisRotTot < 360)
		{
			yAxisRotTot = ((yAxisRotTot + distMidX) % 360);
		}
		else
		{
			yAxisRotTot = 0;
		}
		


		int distMidY = (SCREEN_HEIGHT / 2) - y;
		
		if (xAxisRotTot >= -90 && xAxisRotTot <= 90)
		{
			xAxisRotTot = (xAxisRotTot + distMidY) % 360;
		}

		if(xAxisRotTot < -90)
		{
			xAxisRotTot = -90;
		}
		else if (xAxisRotTot > 90)
		{
			xAxisRotTot = 90;
		}

		lookAtX = sin(float(yAxisRotTot)*(0.0175)) + posX;
		lookAtY = sin(float(xAxisRotTot)*(0.0175)) + posY;
		lookAtZ = cos(float(yAxisRotTot)*(0.0175)) *  cos(float(xAxisRotTot)*(0.0175)) + posZ;
	}
	
}
//openGL keyboard read, containing input control 
void keyboardread(unsigned char key, int x, int y)
{
	switch (key) {
		//'a' strafes the camera left
	case 'a':
		right = maths.multiplyMatVect(rot270, Vector(lookAtX - posX, 0, lookAtZ - posZ));
		length = maths.vectorLength(right);
		right.vect[0] = right.vect[0] / length;
		right.vect[1] = right.vect[1] / length;
		right.vect[2] = right.vect[2] / length;
		posX = posX - right.vect[0];
		posZ = posZ - right.vect[2];
		lookAtX = lookAtX - right.vect[0];
		lookAtZ = lookAtZ - right.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		break;
	case 'A':
		right = maths.multiplyMatVect(rot270, Vector(lookAtX - posX, 0, lookAtZ - posZ));
		length = maths.vectorLength(right);
		right.vect[0] = right.vect[0] / length;
		right.vect[1] = right.vect[1] / length;
		right.vect[2] = right.vect[2] / length;
		posX = posX - right.vect[0];
		posZ = posZ - right.vect[2];
		lookAtX = lookAtX - right.vect[0];
		lookAtZ = lookAtZ - right.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		break;
		//'d' strafes the camera right
	case 'd':
		right = maths.multiplyMatVect(rot90, Vector(lookAtX - posX, 0, lookAtZ - posZ));
		length = maths.vectorLength(right);
		right.vect[0] = right.vect[0] / length;
		right.vect[1] = right.vect[1] / length;
		right.vect[2] = right.vect[2] / length;
		posX = posX - right.vect[0];
		posZ = posZ - right.vect[2];
		lookAtX = lookAtX - right.vect[0];
		lookAtZ = lookAtZ - right.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		glutPostRedisplay();
		break;
	case 'D':
		right = maths.multiplyMatVect(rot90, Vector(lookAtX - posX, 0, lookAtZ - posZ));
		length = maths.vectorLength(right);
		right.vect[0] = right.vect[0] / length;
		right.vect[1] = right.vect[1] / length;
		right.vect[2] = right.vect[2] / length;
		posX = posX - right.vect[0];
		posZ = posZ - right.vect[2];
		lookAtX = lookAtX - right.vect[0];
		lookAtZ = lookAtZ - right.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		glutPostRedisplay();
		break;
		//'s' moves the camera backwards
	case 's':
		fwd = Vector(lookAtX - posX, lookAtY - posY, lookAtZ - posZ);
		length = maths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		posX = posX - fwd.vect[0];
		posY = posY - fwd.vect[1];
		posZ = posZ - fwd.vect[2];
		lookAtX = lookAtX - fwd.vect[0];
		lookAtY = lookAtY - fwd.vect[1];
		lookAtZ = lookAtZ - fwd.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		break;
	case 'S':
		fwd = Vector(lookAtX - posX, lookAtY - posY, lookAtZ - posZ);
		length = maths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		posX = posX + fwd.vect[0];
		posY = posY + fwd.vect[1];
		posZ = posZ + fwd.vect[2];
		lookAtX = lookAtX - fwd.vect[0];
		lookAtY = lookAtY - fwd.vect[1];
		lookAtZ = lookAtZ - fwd.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		break;
		//'w' moves the camera forward
	case 'w':
		fwd = Vector(lookAtX - posX, lookAtY - posY, lookAtZ - posZ);
		length = maths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		posX = posX + fwd.vect[0];
		posY = posY + fwd.vect[1];
		posZ = posZ + fwd.vect[2];
		lookAtX = lookAtX + fwd.vect[0];
		lookAtY = lookAtY + fwd.vect[1];
		lookAtZ = lookAtZ + fwd.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		break;
	case 'W':
		fwd = Vector(lookAtX - posX, lookAtY - posY, lookAtZ - posZ);
		length = maths.vectorLength(fwd);
		fwd.vect[0] = fwd.vect[0] / length;
		fwd.vect[1] = fwd.vect[1] / length;
		fwd.vect[2] = fwd.vect[2] / length;
		posX = posX + fwd.vect[0];
		posY = posY + fwd.vect[1];
		posZ = posZ + fwd.vect[2];
		lookAtX = lookAtX + fwd.vect[0];
		lookAtY = lookAtY + fwd.vect[1];
		lookAtZ = lookAtZ + fwd.vect[2];
		//printf("POS X: %f Y: %f Z: %f\n", posX, posY, posZ);
		glutPostRedisplay();
		break;
		//'y' is the pause toggle for enemy AI - pausing the AI essentially just stops them moving
	case 'y':
		for (int i = 0; i < map.enemies; i++)
		{
			if (enemies[i].enemyPause)
			{
				enemies[i].enemyPause = false;
			}
			else
			{
				enemies[i].enemyPause = true;
			}
		}
		glutPostRedisplay();
		break;
	case 'Y':
		for (int i = 0; i < map.enemies; i++)
		{
			if (enemies[i].enemyPause)
			{
				enemies[i].enemyPause = false;
			}
			else
			{
				enemies[i].enemyPause = true;
			}
		}
		glutPostRedisplay();
		break;
		//p toggles the cursor being active
	case 'p':
		if (lockMouse)
		{
			lockMouse = false;
		}
		else
		{
			lockMouse = true;
		}
		glutPostRedisplay();
		break;
	case 'P':
		if (lockMouse)
		{
			lockMouse = false;
		}
		else
		{
			lockMouse = true;
		}
		glutPostRedisplay();
		break;
		//t toggles the player AI on and off
	case 't':
		if (AIPlayer.worldPause)
		{
			AIPlayer.worldPause = false;
		}
		else
		{
			AIPlayer.worldPause = true;
		}
		glutPostRedisplay();
		break;
	case 'T':
		if (AIPlayer.worldPause)
		{
			AIPlayer.worldPause = false;
		}
		else
		{
			AIPlayer.worldPause = true;
		}
		glutPostRedisplay();
		break;
		//# changes which influence is being displayed on the map
	case '#':
		if (infViewMode != 4)
		{
			infViewMode++;
		}
		else
		{
			infViewMode = 0;
		}
		glutPostRedisplay();
		break;
		//[ and ] increase and decrease the time between ai updates
	case '[':
		if (TIMER != 10)
		{
			TIMER--;
		}
		break;
	case ']':
		TIMER++;
		break;
		//. immediately resets update timer to lowest value
	case '.':
		TIMER = 10;
		break;
		//, positions the camera above the map so that you can see the majority of it, in case the camera controls are too cumbersome
	case ',':
		lockMouse = false;
		posX = (map.width / 2) * 5;
		posZ = (map.height / 2) * 5;
		posY = 100;
		lookAtX = posX;
		lookAtZ = posZ;
		lookAtY = 100;
		maths.setRotZMat(&rotation, 270);
		newLookAt = Vector(lookAtX, lookAtY, lookAtZ);
		length = maths.vectorLength(newLookAt);
		newLookAt = maths.multiplyMatVect(rotation, newLookAt);
		lookAtX = newLookAt.vect[0];
		lookAtY = newLookAt.vect[1];
		lookAtZ = newLookAt.vect[2];
		break;
	}
}


//openGL init function
void init(void)
{
	//initialise rotation matricies that are used frequently
	maths.setRotYMat(&rot90, 90);
	maths.setRotYMat(&rot270, 270);
	//OpenGL initialisation goes here
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glutPassiveMotionFunc(rotCam);
	GLfloat mat_specular[] = { 1, 1, 1.0, 1.0 };
	GLfloat mat_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_ambient[] = { 1, 1, 1, 1.0 };
	GLfloat mat_shininess[] = { 12.8 };
	//glEnable(GL_LIGHTING);
	//glEnable(GL_LIGHT0);
	GLfloat light_position[] = { 0.5, 5 , 0.5, 0.0 };

	glClearColor(0.0, 0.0, 0.0, 0.0);  //reset "empty" background colour
	glShadeModel(GL_SMOOTH);

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);


	//glShadeModel(GL_SMOOTH);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glFrontFace(GL_CCW);
	glClearDepth(1.0f);
	glutInitDisplayMode(GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//call looping function at start of program to keep running throughout
	glutTimerFunc(1, round, 0);
}

//draw object from loaded obj object (assumes triangulation already occured)
void drawObj(obj object)
{
	for (int i = 0; i < object.fSize; i++)
	{
		glBegin(GL_TRIANGLES);
		glVertex3f(object.vList[object.fList[i].v1].x, object.vList[object.fList[i].v1].y, object.vList[object.fList[i].v1].z);
		glVertex3f(object.vList[object.fList[i].v3].x, object.vList[object.fList[i].v3].y, object.vList[object.fList[i].v3].z);
		glVertex3f(object.vList[object.fList[i].v2].x, object.vList[object.fList[i].v2].y, object.vList[object.fList[i].v2].z);
		glEnd();
	}
}

//draw object at a specified position
void drawObjAt(obj object, float xPos, float yPos, float zPos)
{
	for (int i = 0; i < object.fSize; i++)
	{
		glBegin(GL_TRIANGLES);
		glVertex3f(object.vList[object.fList[i].v1].x + xPos, object.vList[object.fList[i].v1].y + yPos, object.vList[object.fList[i].v1].z + zPos);
		glVertex3f(object.vList[object.fList[i].v3].x + xPos, object.vList[object.fList[i].v3].y + yPos, object.vList[object.fList[i].v3].z + zPos);
		glVertex3f(object.vList[object.fList[i].v2].x + xPos, object.vList[object.fList[i].v2].y + yPos, object.vList[object.fList[i].v2].z + zPos);
		glEnd();
	}
}

//draws a loaded map, specific function to read codes and draw appropriate object
void drawMap(Map inMap)
{
	//for each row
	for (int i = 0; i < inMap.height; i++)
	{
		//for each column
		for (int j = 0; j < inMap.width; j++)
		{
			//influence trackers
			float inf = 0;
			float inf2 = 0;
			//ground is 5 units wide
			switch (inMap.tileList[(i*inMap.width) + j].code)
			{
			default:
				break;
				//code 0 is a wall
			case 0:
				//set wall colour
				glColor3f(1, 0, 1);
				//draw at current position multiplied by 5 to accomodate object size
				drawObjAt(Wall, inMap.tileList[(i*inMap.width) + j].position.x * 5, 0, inMap.tileList[(i*inMap.width) + j].position.y * 5);
				break;
				//code 1 is a floor tile
			case 1:
				//depending on which influence we wish to view, or none at all, set colour of floor tile according to the tiles influence value
				if (infViewMode == 0)
				{
					//all solid red for no influence
					glColor3f(1, 0, 0);
				}
				else if (infViewMode == 1)
				{
					//according to enemy influence
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				else if (infViewMode == 3)
				{
					//according to both enemy and goal influence
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					inf2 = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, inf2);
				}
				else if (infViewMode == 4)
				{
					//according to visited tile influence
					inf = (inMap.tileList[(i*inMap.width) + j].visited * 2) / 100.0f;
					if (inMap.tileList[(i*inMap.width) + j].visited >= 50)
					{
						//once tile has been visited 50 times, colour red
						glColor3f(1, 0, 0);
					}
					else
					{
						//depending how close to visited 50 times, colour a shade of yellow
						glColor3f(inf, inf, 0);
					}
				}
				else
				{
					//according to goal's influence
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				//draw the ground tile
				drawObjAt(Ground, inMap.tileList[(i*inMap.width) + j].position.x * 5, 0, inMap.tileList[(i*inMap.width) + j].position.y * 5);
				break;
				//code 2 is a player object, however when drawing the map it is treated the same as a ground tile as above
			case 2:
				if (infViewMode == 0)
				{
					glColor3f(1, 0, 0);
				}
				else if (infViewMode == 1)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				else if (infViewMode == 3)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					inf2 = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, inf2);
				}
				else if (infViewMode == 4)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].visited * 2) / 100.0f;
					glColor3f(inf, inf, 0);
				}
				else
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				drawObjAt(Ground, inMap.tileList[(i*inMap.width) + j].position.x * 5, 0, inMap.tileList[(i*inMap.width) + j].position.y * 5);
				break;
				//code 3 is an enemy object, however when drawing the map it is treated the same as a ground object as above
			case 3:
				if (infViewMode == 0)
				{
					glColor3f(1, 0, 0);
				}
				else if (infViewMode == 1)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				else if (infViewMode == 3)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					inf2 = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, inf2);
				}
				else if (infViewMode == 4)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].visited * 2) / 100.0f;
					glColor3f(inf, inf, 0);
				}
				else
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				drawObjAt(Ground, inMap.tileList[(i*inMap.width) + j].position.x * 5, 0, inMap.tileList[(i*inMap.width) + j].position.y * 5);
				break;
				//code 4 is a goal, we draw a tile object as above and then draw the goal object on top
			case 4:
				//drawing goal object first
				glColor3f(0, 1, 0);
				drawObjAt(Goal, inMap.tileList[(i*inMap.width) + j].position.x * 5, 0, inMap.tileList[(i*inMap.width) + j].position.y * 5);
				//then drawing tile object (same as above)
				if (infViewMode == 0)
				{
					glColor3f(1, 0, 0);
				}
				else if (infViewMode == 1)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				else if (infViewMode == 3)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[1] / 100.0f);
					inf2 = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, inf2);
				}
				else if (infViewMode == 4)
				{
					inf = (inMap.tileList[(i*inMap.width) + j].visited * 2) / 100.0f;
					glColor3f(inf, inf, 0);
				}
				else
				{
					inf = (inMap.tileList[(i*inMap.width) + j].infIndex[2] / 100.0f);
					glColor3f(inf, 0, 0);
				}
				drawObjAt(Ground, inMap.tileList[(i*inMap.width) + j].position.x * 5, 0, inMap.tileList[(i*inMap.width) + j].position.y * 5);
				break;
			}
		}
	}
}

//search for player object in a loaded map to pass appropriate values to an AI controller
void searchPlayer(Map inMap, AIcontroller* inBot)
{
	for (int i = 0; i < inMap.height; i++)
	{
		for (int j = 0; j < inMap.width; j++)
		{
			//pass in tile code to switch
			switch (inMap.tileList[(i*inMap.width) + j].code)
			{
			default:
				break;
				//if tile code matches player code, we found the player
			case 2:
				//pass positioninto the provided controller
				inBot->Pos.x = j;
				inBot->Pos.y = i;
				break;
			}
		}
	}
}

//search for goal object to pass into bot
void searchGoal(Map inMap, AIcontroller* inBot)
{
	for (int i = 0; i < inMap.height; i++)
	{
		for (int j = 0; j < inMap.width; j++)
		{
			switch (inMap.tileList[(i*inMap.width) + j].code)
			{
			default:
				break;
				//if tile code is goal code, goal is found
			case 4:
				//pass goal position to provided bot
				inBot->Goal.x = j;
				inBot->Goal.y = i;
				break;
			}
		}
	}
}

//openGL reshape function
void reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1.0, 1.0, -1.0, 1.0, 1.5, 2000.0);
	//l,r,botoom,top,near,far
	glMatrixMode(GL_MODELVIEW);
}


//calc influence looks at all influence emitters and spreads influence to surrounding tiles appropriately
void calcInf(Map* inMap)
{
	//two loops to step through every influence on every tile
	for (int i = 0; i < inMap->height * inMap->width; i++)
	{
		for (int j = 0; j < inMap->totInf; j++)
		{
			//pass loop counter into switch to work for each influence
			float newinf = 0;
			switch (j)
			{
			default:
				break;
			case 0:
				break;
				//for enemy influence...
			case 1:
				//for every enemy in the map...
				for (int k = 0; k < inMap->enemies; k++)
				{
					//work out the distance from the enemy we're looking at
					float distance = sqrt(((inMap->tileList[i].position.x - enemies[k].Pos.x)*(inMap->tileList[i].position.x - enemies[k].Pos.x)) + ((inMap->tileList[i].position.y - enemies[k].Pos.y)*(inMap->tileList[i].position.y - enemies[k].Pos.y)));
					//calculate influence using a formula that lets influence drop off drastically after a few tiles, add it to the tiles current influence
					//this way the enemy influence accumulates depending on how many enemies are near enough to it
					newinf = newinf + ( 100 / (pow(e, distance - 3) + 1));
					
				}
				//place influence value on this tile
				inMap->tileList[i].infIndex[1] = newinf;
				break;
				//for the goal's influence...
			case 2:
				//work out the tile's distance from the goal
				float distance = sqrt(((inMap->tileList[i].position.x - inMap->goal.position.x) * (inMap->tileList[i].position.x - inMap->goal.position.x)) + ((inMap->tileList[i].position.y - inMap->goal.position.y) * (inMap->tileList[i].position.y - inMap->goal.position.y)));
				float newInf = 0;
				//if the tile isn't the tile the goal is on...
				if (distance != 0)
				{
					//tile's influence is the riciprocal of (distance + 0.05). Influence is not clamped so it can spread across the whole map
					newInf = 250 * (1 /(distance+0.05));
				}
				else
				{
					//the tile the goal is on is full influence
					newInf = 250;
				}
				//place this influence on this tile
				inMap->tileList[i].infIndex[2] = newInf;
				break;
			}
		}
	}
}

//openGL display function
void display()
{
	//clear background colour, so as last drawn buffer isn't shown
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	glLoadIdentity();
	//gluLookAt(camDist*sin(float(theta)*0.0175) + strafe, camDist, camDist*cos(float(theta)*0.0175), lookAtX + strafe, lookAtY, lookAtZ, 0.0, 1.0, 0.0);
	gluLookAt(posX, posY, posZ, lookAtX + strafe, lookAtY, lookAtZ, 0.0, 1.0, 0.0);
	//first three - place camera, second three, aim camera at, last three, up vector


	//we can add any functions that draw objects here
	//calculate the influence...
	calcInf(&map);
	//draw the map...
	drawMap(map);
	//set colour to draw friend entity
	glColor3f(0, 1, 0);
	//draw player entity
	drawObjAt(Entity, AIPlayer.Pos.x * 5, 0, AIPlayer.Pos.y * 5);
	//set colour for enemy entities
	glColor3f(1, 1, 0);
	//draw all enemies
	for (int i = 0; i < map.enemies; i++)
	{
		drawObjAt(Entity, enemies[i].Pos.x * 5, 0, enemies[i].Pos.y * 5);
	}

	glutSwapBuffers();
}

//clear enemy influences so we can spread new influences based on position
void cleanEnemyInf(Map* inMap)
{
	//step through each tile...
	for (int i = 0; i < inMap->height * inMap->width; i++)
	{
		//make enemy influence 0...
		inMap->tileList[i].infIndex[1] = 0;
		for (int j = 0; j < inMap->enemies; j++)
		{
			//make every tile an enemy is stood on the full value...
			if (enemies[j].Pos.x == inMap->tileList[i].position.x && enemies[j].Pos.y == inMap->tileList[i].position.y)
			{
				inMap->tileList[i].infIndex[1] = 100;
			}
		}
	}
}


void main(int argc, char** argv)
{
	//MAP LOADING
	MapLoader loader;
	//load map
	map = loader.LoadMap("TestMap.txt");
	//set array of enemies to hold the right amount of enemies, default constructor of an AI controller assumes we are creating an enemy AI
	enemies = new AIcontroller[map.enemies];
	//set enemy AI attributes accordingly
	for (int i = 0; i < map.enemies; i++)
	{
		enemies[i].enemy = true;
		enemies[i].map = &map;
		enemies[i].Pos.x = map.enemyList[i].position.x;
		enemies[i].Pos.y = map.enemyList[i].position.y;
		enemies[i].enemyPause = true;
	}

	//OBJ LOADING
	objNewReader objReader;
	//loading in objects for visual representation
	Ground = objReader.readObjFile("ground.obj");
	Wall = objReader.readObjFile("wall.obj");
	Entity = objReader.readObjFile("entity.obj");
	Goal = objReader.readObjFile("goalNut.obj");

	//search for player and goal in map codes to set up AI correctly
	searchPlayer(map, &AIPlayer);
	searchGoal(map, &AIPlayer);


	//OpenGL Setup
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutCreateWindow("Influence Maps");
	init();
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboardread);
	glutMainLoop();
}