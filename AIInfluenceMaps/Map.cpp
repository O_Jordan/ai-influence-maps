#include "Map.h"
//initlaises lists and fills them with blank objects ready to be written to
Map::Map(int width, int height, int infCount, int enemyCount)
{
	tileList = new tile[width*height];
	enemyList = new enemy[enemyCount];
	this->width = width;
	this->height = height;
	totInf = 0;
	enemies = enemyCount;
	goal.position.x = 0;
	goal.position.y = 0;
	tile clean;
	clean.code = 'q';
	clean.position.x = 0;
	clean.position.y = 0;
	clean.visited = 0;
	for (int i = 0; i < infCount; i++)
	{
		clean.infIndex[i] = 0;
	}

	for (int j = 0; j < width*height; j++)
	{
		tileList[j] = clean;
	}

}

Map::~Map()
{
}
