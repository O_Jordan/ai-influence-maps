#pragma once
//map holds level data
class Map 
{
public:
	//creates object, requires variablesso normally initialised with 1,1,1,1
	Map(int width, int height, int infCount, int enemyCount);
	~Map();

	//size of map
	int width;
	int height;
	//number of enemies on the map
	int enemies;
	//total number of influences on the map
	int totInf;
	//position structure
	struct pos
	{
		int x;
		int y;
	};
	//enemy structure
	struct enemy
	{
		pos position;
	};
	//goal structure
	struct goal
	{
		pos position;
	};
	//holds goal information
	goal goal;
	//tile structure, contains information about each tile in the map
	//tile code is what sort of tile this is
	//array infindex stores that tiles influence values
	//visited stores how many times the Player AI visited this tile
	//position is the tile position in the level
	struct tile
	{
		int code;
		float infIndex[3];
		int visited;
		pos position;
	};
	//create a list of tiles and enemies
	tile * tileList;
	enemy * enemyList;
private:

};
