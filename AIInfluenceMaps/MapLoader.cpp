#include "MapLoader.h"

MapLoader::MapLoader()
{
}

MapLoader::~MapLoader()
{
}

Map MapLoader::LoadMap(char* mapLocation)
{

	//THE MAP FILES BEING USED STORE METADATA. THIS LINE IS STORED AS "#DATA" FOLLOWED BY THREE VALUES - HEIGHT WIDTH AND THE NUMBER OF INFLUENCE EMITTER TYPES
	char buffer[100];
	FILE * mapFile;
	int width;
	int height;
	int infCount;
	int enemyCount;

	//open file
	mapFile = fopen(mapLocation, "r");
	//scan in first line
	fscanf(mapFile, "%s", buffer);

	//loop till eof
	//seems a lil wasteful to have to scan the file twice for just the metadata- FIX THIS IF POSSIBLE AND TIME ALLOWS
	while (!feof(mapFile))
	{
		if (buffer != "")
		{
			if (strcmp(buffer, "#data") == 0)
			{
				//we're in metadata, we know what the next three scans should be
				fscanf(mapFile, "%d %d %d %d", &height, &width, &infCount, &enemyCount);
			}
		}
		fscanf(mapFile, "%s", buffer);
	}

	//init Map
	Map outMap(width, height, infCount, enemyCount);
	//return to start of file
	rewind(mapFile);
	int wCounter = 0;
	int hCounter = 0;
	outMap.totInf = infCount;
	fscanf(mapFile, "%s", buffer);
	int enemyCounter = 0;
	while (!feof(mapFile))
	{
		
		if (buffer != "")
		{
			//a '~' signifies a line follows that is a map line, otherwise we ignore it...
			if (strcmp(buffer, "~") == 0)
			{
				//for each character in the line read in, check character and set map's tile code accordingly...
				fscanf(mapFile, "%s", buffer);
				for (int i = 0; i < width; i++)
				{
					switch (buffer[i])
					{
						//character x is a wall
					case 'x':
						outMap.tileList[(hCounter * width) + wCounter].code = 0;
						outMap.tileList[(hCounter * width) + wCounter].infIndex[0] = 1000;
						break;
						//character . is a lfoor tile
					case '.':
						outMap.tileList[(hCounter * width) + wCounter].code = 1;
						break;
						//character p is a player
					case 'p':
						outMap.tileList[(hCounter * width) + wCounter].code = 2;
						break;
						//character e is an enemy
					case 'e':
						outMap.tileList[(hCounter * width) + wCounter].code = 3;
						outMap.tileList[(hCounter * width) + wCounter].infIndex[1] = 100;
						outMap.enemyList[enemyCounter].position.x = wCounter;
						outMap.enemyList[enemyCounter].position.y = hCounter;
						enemyCounter++;
						break;
						//character g is a goal
					case 'g':
						outMap.tileList[(hCounter * width) + wCounter].code = 4;
						outMap.tileList[(hCounter * width) + wCounter].infIndex[2] = 250;
						outMap.goal.position.x = wCounter;
						outMap.goal.position.y = hCounter;
						break;
					}
					//place tile's position correctly
					outMap.tileList[(hCounter * width) + wCounter].position.x = wCounter;
					outMap.tileList[(hCounter * width) + wCounter].position.y = hCounter;
					wCounter++;
				}
				hCounter++;
				wCounter = 0;
			}
		}
		fscanf(mapFile, "%s", buffer);
	}

	return outMap;
}
