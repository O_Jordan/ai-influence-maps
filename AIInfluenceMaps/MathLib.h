#pragma once
#include <math.h>
#include "Matrix.h"
#include "Vector.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////																													   ////
////	MATHLIB WRITTEN FOR PREVIOUS ASSIGNMENT IN SECOND YEAR. REUSED AS IS WITH NO CHANGES FROM THERE. ALL WORK MINE.    ////
////																													   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class MathLib
{
public:
	MathLib();
	~MathLib();

	float lupCos[1024];
	float lupSin[1024];

	float conversionAngRad(float degrees);
	float conversionAngDeg(float Radians);

	float getCos(float angleInDegrees);
	float getSin(float angleInDegrees);
	Vector multiplyMatVect(Matrix inMat, Vector inVect);
	void setIDMat(Matrix * inMat);
	void setTransMat(Matrix * inMat, float x, float y, float z);
	void setRotXMat(Matrix * inMat, float angle);
	void setRotYMat(Matrix * inMat, float angle);
	void setRotZMat(Matrix * inMat, float angle);
	void setUniSclMat(Matrix * inMat, float scale);
	void setSclMat(Matrix * inMat, float scaleX, float scaleY, float scaleZ);
	Matrix multiplyMat(Matrix m1, Matrix m2);
	Matrix addMat(Matrix m1, Matrix m2);

	Matrix transposeMat(Matrix inputMat);
	bool checkOrth(Matrix inputMat);

	float dotProd(Vector v1, Vector v2);
	float vectorLength(Vector inVect);


private:
	const float PI = 3.14;
};
