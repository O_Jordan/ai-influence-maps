#pragma once
class AIcontroller;
//sets virtual functions for states
class State
{
public:

	virtual void Execute(AIcontroller*) = 0; //0 sets virtual void as not implemented

	virtual void Enter(AIcontroller*) = 0;

	virtual void Exit(AIcontroller*) = 0;
};