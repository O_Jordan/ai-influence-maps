#pragma once


//stores object details
class obj
{
private:
public:
	obj(int inVSize, int inVTSize, int inVNSize, int inFSize);
	~obj();

	//stores count of each data type
	int vSize;
	int vtSize;
	int vnSize;
	int fSize;

	struct Vertex
	{
		float x, y, z;
	};

	struct vTexture
	{
		float x, y;
	};

	struct vNormal
	{
		float x, y, z;
	};

	struct Face
	{
		int v1, v2, v3;
		int vt1, vt2, vt3;
		int vn1, vn2, vn3;
	};

	//lists for each type
	Vertex * vList;
	vTexture * vtList;
	vNormal * vnList;
	Face * fList;
};

