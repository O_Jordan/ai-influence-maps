#include "objNewReader.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////																													   ////
////	OBJREADER WRITTEN AS AN ALL PURPOSE LIBRARY, USED IN MULTIPLE THIRD YEAR PROJECTS. ALL WORK MINE.                  ////
////																													   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
objNewReader::objNewReader()
{
}

objNewReader::~objNewReader()
{
}

obj objNewReader::readObjFile(char * inFile)
{
	//buffer for reading
	char buffer[50];
	FILE * objFile;
	int vCount = 0;
	int vtCount = 0;
	int vnCount = 0;
	int fCount = 0;
	//unsure if used
	int faceCornerCount;
	bool inFace = false;
	bool didRead = false;

	//open file
	objFile = fopen(inFile, "r");
	//scan in first line
	fscanf(objFile, "%s", buffer);

	//looping while not at EOF
	while (!feof(objFile))
	{
		//if buffer isn't whitespace, move on
		if (buffer != "")
		{
			//if buffer matches certain keys, take action
			if (strcmp(buffer, "v") == 0)
			{
				vCount++;
			}
			else if (strcmp(buffer, "vt") == 0)
			{
				vtCount++;
			}
			else if (strcmp(buffer, "vn") == 0)
			{
				vnCount++;
			}
			else if (strcmp(buffer, "f") == 0)
			{
				fCount++;
			}
			//here we would also check for mtls, but we're only fussed with mesh data right now
		}
		fscanf(objFile, "%s", buffer);
	}

	//internal obj to hole data and output when we're done
	obj object(vCount, vtCount, vnCount, fCount);
	//would also have read in mtlfile name from file in first pass and open it here

	//return to start of objfile
	rewind(objFile);

	//we're tracking data being put in to make sure it matches what was first counted here
	//also tracks which slot in the object array to dump data in
	int vIn = 0;
	int vtIn = 0;
	int vnIn = 0;
	int fIn = 0;

	//here we would usually read in all the mtls and assign an ID to them. No need this time
	//read ahead in objfile
	fscanf(objFile, "%s", buffer);
	//while not at EOF again
	while (!feof(objFile))
	{
		//if string matches, copy data over
		//Wanted to use a case switch instead for this in previous version?? Check this
		if (strcmp(buffer, "v") == 0)
		{
			//if we see a v we know the next three are point data
			fscanf(objFile, "%f %f %f", &object.vList[vIn].x, &object.vList[vIn].y, &object.vList[vIn].z);
			//increase count
			vIn++;
		}
		else if (strcmp(buffer, "vt") == 0)
		{
			fscanf(objFile, "%f %f", &object.vtList[vtIn].x, &object.vtList[vtIn].y);
			vtIn++;
		}
		else if (strcmp(buffer, "vn") == 0)
		{
			fscanf(objFile, "%f %f %f", &object.vnList[vnIn].x, &object.vnList[vnIn].y, &object.vnList[vnIn].z);
			vnIn++;
		}
		//a check for usemtl exists in previous version - no need here
		else if (strcmp(buffer, "f") == 0)
		{
			//HEY! WOULD IT BE TOO MUCH TO DECOUPLE THIS?
			//previous version worked with quads - working with tris here
			//previous versions assigned mtlindex to faces - only concerned with mesh for now, not mtl
			
			//face data is formatted as f int/int/int int/int/int int/int/int
			//each set of three is v/vt/vn, refers to each vertex of a tri
			//following the f tag in file, we know three sets occur
			//so we read in each set, split that set, copy data

			fscanf(objFile, "%s", buffer);
			//strtok needs to output data to somewhere, so we make that somewhere
			char * split = strtok(buffer, "/");
			//then copy this!
			//gotta convert the data to int tho, and drop by one to count from 0. yay for zeroes.
			object.fList[fIn].v1 = atoi(split) - 1;
			//strtok remembers the set it's splitting - we use NULL to let it carry on through the data
			//overwriting split for each new strtok operation since we've gotten the data we need from it
			split = strtok(NULL, "/");
			object.fList[fIn].vn1 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vt1 = atoi(split) - 1;

			//and now we do it all again for sets 2 and 3
			fscanf(objFile, "%s", buffer);
			split = strtok(buffer, "/");
			object.fList[fIn].v2 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vn2 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vt2 = atoi(split) - 1;

			fscanf(objFile, "%s", buffer);
			split = strtok(buffer, "/");
			object.fList[fIn].v3 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vn3 = atoi(split) - 1;
			split = strtok(NULL, "/");
			object.fList[fIn].vt3 = atoi(split) - 1;

			//put that count up
			fIn++;
			//here previous version basically checked if an object was using quads and triangulated it into a new face
			//project isn't as fussed about that right now. Not bothering
		}
		fscanf(objFile, "%s", buffer);
	}
	//close that file
	fclose(objFile);
	//get that object out into the world!
	//i.e. return object
	return object;
	//HEY. YOU'VE COUNTED HOW MANY DATA YOU COPIED. OUGHT TO ERROR CHECK IT BEFORE OUTPUT
}
