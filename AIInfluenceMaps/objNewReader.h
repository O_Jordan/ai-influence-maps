#pragma once
#include "obj.h"
#include <iostream>
#include <string>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////																													   ////
////	OBJREADER WRITTEN AS AN ALL PURPOSE LIBRARY, USED IN MULTIPLE THIRD YEAR PROJECTS. ALL WORK MINE.                  ////
////																													   ////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class objNewReader
{
private:

public:
	objNewReader();
	~objNewReader();

	//read file, pass back resulting obj from file
	obj readObjFile(char * inFile);
};
